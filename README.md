==== Lumber Mill service ====

# Introduction
Lumber mills process raw timber into lumber which can then be used to construct houses and other buildings that are primarily made of wood. The current lumber mills have a 45% loss rate when converting raw timber to usable lumber. 1 ton of timber is processed every 2 days. However, a lumber mill can only store 50 tons of timber and finished lumber at any given time. Processed lumber should be moved to storage as soon as possible to avoid production stoppages.


## Features


## Folder Structure
* `README.md` -> Readme for the project. This should be updated for each project. This readme file can be used as a templated
* `CHANGELOG.md` -> Document the code changes here
* `index.js` -> Entry point of the app
* `broker.js` -> Setup code for the Moleculer. This could be moved to a separate package in the future
* `services/api/` -> Contains application services
* `services/core/` -> Contains common services. This could be moved to a separate package in the future


# Usage
* `npm install`
* `npm run dev` to start development code
* `npm run build` to build production code
* `npm start` to start production code. Note that when running in a docker container, run `node index.js` directly instead of an `npm` command

# Future Work
