import {ServiceBroker} from 'moleculer';
import coreServices from './services/core';

let broker;

async function initBroker({gateway, database, preloadServices}) {
  gateway = typeof gateway === 'undefined' ? true : gateway;
  database = typeof database === 'undefined' ? true : database;
  preloadServices = typeof preloadServices === 'undefined' ? [] : preloadServices;

  broker = new ServiceBroker();
  if (gateway) broker.createService(coreServices.GatewayService);
  if (database) broker.createService(coreServices.DatabaseClientService);
  preloadServices.map((services) => createServices(services));
}

async function startBroker() {
  await broker.start();
}

async function createServices(res) {
  Object.keys(res).map(async (f) => {
    await broker.createService(res[f]);
  });
}

export {broker, initBroker, startBroker, createServices};
