import {Service as MoleculerService} from 'moleculer';
import {Service, Action} from 'moleculer-decorators';

import axios from 'axios';

const NAME = 'DatabaseClient';

@Service({
  name: NAME,
  settings: {
    serviceUrl: process.env.DB_SERVICE_URL,
    username: process.env.DB_SERVICE_ID,
    password: process.env.DB_SERVICE_KEY,
  },
})
class DatabaseClientService extends MoleculerService {
  @Action({
    params: {
      endpoint: {type: 'string'},
      data: {type: 'object'},
    },
  })
  async request(ctx) {
    try {
      const url = `${this.settings.serviceUrl}${ctx.params.endpoint}`;
      var result = await axios({
        headers: {'Content-Type': 'application/json'},
        method: 'post',
        url: url,
        auth: {
          username: this.settings.username,
          password: this.settings.password,
        },
        data: ctx.params.data,
      });

      return result.data;
    } catch (e) {
      //TODO: Unhandled Error: network error:  1645980232466 { error: true, error_code: 404, error_msg: 'Not Found' }

      if ('errno' in e) {
        // e.errno -111 network connectivity error
        if (e.errno == -111) {
          console.log('network error: ', new Date().toISOString(), {
            error: true,
            error_code: 111,
            error_msg: e.message,
          });
          return {error: true, error_code: 111, error_msg: e.message};
        } else {
          // New error - not witnessed before
          console.log('network error: ', new Date().toISOString(), {
            error: true,
            error_code: e.errno,
            error_msg: e.message,
          });
          return {error: true, error_code: e.errno, error_msg: e.message};
        }
      }

      if ('response' in e) {
        // Authorization failure
        if (e.response.status == 401) {
          console.log('network authorization error: ', Date.now(), {
            error: true,
            error_code: e.response.status,
            error_msg: e.response.statusText,
          });
          return {error: true, error_code: e.response.status, error_msg: e.response.statusText};
        } else {
          // New error - not witnessed before
          console.log('network error: ', Date.now(), {
            error: true,
            error_code: e.response.status,
            error_msg: e.response.statusText,
          });
          return {error: true, error_code: e.response.status, error_msg: e.response.statusText};
        }
      }
    }
  }
}

export default DatabaseClientService;
