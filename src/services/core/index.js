import GatewayService from './Gateway.service';
import DatabaseClientService from './DatabaseClient.service';

export default {GatewayService, DatabaseClientService};
