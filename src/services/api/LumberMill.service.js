import {Service as MoleculerService} from 'moleculer';
import {Service, Action} from 'moleculer-decorators';
import {verifySignature} from '@utils/helpers';

const NAME = 'LumberMill';

@Service({
  name: NAME,
  settings: {},
})
class LumberMillAPIService extends MoleculerService {
  @Action({
    rest: {
      method: 'POST',
    },
    params: {
      message: 'string',
      hash: 'string',
    },
  })
  async deploy(ctx) {
    const {message, hash} = ctx.params;
    console.log(ctx.params);
    const address = verifySignature(message, hash).toLowerCase();
    const messageJson = JSON.parse(message);

    const lumbermill = await ctx.broker.call('DatabaseClient.request', {
      endpoint: '/current_state',
      data: {
        collection: 'buildings',
        query: {serial: messageJson.serial_number, owner: address},
      },
    });

    //ToDo: ** Add check so that if the lumbermill is already deployed in the intended location, there is no need to do anything.

    if (lumbermill.length == 0) {
      return {
        error: true,
        error_code: 0,
        error_msg: 'This building item does not have your address listed as owner.',
      };
    } else if (lumbermill.length == 1) {
      const landData = await ctx.broker.call('DatabaseClient.request', {
        endpoint: '/query',
        data: {
          collection: 'land_data',
          query: {owner: address, plot_int: parseInt(messageJson.new_location)},
          sort: {owner: 1},
        },
      });

      if (landData.length == 0) {
        return {error: true, error_code: 1, error_msg: "You don't own this plot of land."};

        //ToDo: query should never return more than 1 object, but case needs to be covered just in case.
      } else {
        lumbermill[0].timestamp = Date.now() * 1000;
        lumbermill[0].status = 'deployed';
        lumbermill[0].new_location = messageJson.new_location;
        delete lumbermill[0]._id;

        await ctx.broker.call('DatabaseClient.request', {
          endpoint: '/query',
          data: {
            collection: 'buildings',
            entry_doc: lumbermill[0],
          },
        });

        //ToDo: User input for the query is very dangerous. Find something else.

        landData[0].template = landData[0].template.replace('Forest', 'Building');

        const check = await ctx.broker.call('DatabaseClient.request', {
          endpoint: '/insert_single',
          data: {
            collection: 'land_data',
            entry_doc: {
              timestamp: Date.now(),
              land_status: 'building',
              flash_over: true,
              img_url: 'https://eep.province.gg/images/bulldozer.png',
              template: landData[0].template,
            },
          },
        });

        if (check.acknowledged == true) {
          return {error: false};
        } else {
          return {error: true, error_code: 1, error_msg: "You don't have permission to put a building on this plot."};
        }
      }
    }
  }

  @Action({
    rest: {
      method: 'POST',
    },
    params: {
      serial: 'string',
    },
  })
  async run(ctx) {
    console.log(ctx.params);
    return 'running';
  }

  @Action({
    rest: {
      method: 'GET',
    },
    params: {
      serial: 'string',
    },
  })
  async status(ctx) {
    console.log(ctx.params);
    return 'status';
  }
}

export default LumberMillAPIService;
